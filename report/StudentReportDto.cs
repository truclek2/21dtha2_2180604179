﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.models;

namespace WindowsFormsApp1.report
{
    internal class StudentReportDto
    {
        public string StudentID { get; set; }

       
        public string FullName { get; set; }

        public double AverageScore { get; set; }

        public string FacultyName { get; set; }
      
    }
}
