﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.models;
using System.Data.Common;
using System.Security.Cryptography;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                StudentContexDB context = new StudentContexDB();
                List<Faculty> listFacuty = context.Faculties.ToList();
                List<Student> listStudent = context.Students.ToList();
                FillFacultyCombobox(listFacuty);
                BindGrid(listStudent);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void FillFacultyCombobox(List<Faculty> listfaculties)
        {
            this.comboBox1.DataSource = listfaculties;
            this.comboBox1.DisplayMember = "FacultyName";
            this.comboBox1.ValueMember = "FacultyID";
        }
        private void BindGrid(List<Student> listStudents)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in listStudents)
            {
                int index = dataGridView1.Rows.Add(item);
                dataGridView1.Rows[index].Cells[0].Value = item.StudentID;
                dataGridView1.Rows[index].Cells[1].Value = item.FullName;
                dataGridView1.Rows[index].Cells[2].Value = item.Faculty.FacultyName;
                dataGridView1.Rows[index].Cells[3].Value = item.AverageScore;
            }
        }

        private void dgvStudent_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {

                string Masv = dataGridView1.Rows[e.RowIndex].Cells["Masv"].Value.ToString();
                string Hoten = dataGridView1.Rows[e.RowIndex].Cells["Hoten"].Value.ToString();
                string nameKhoa = dataGridView1.Rows[e.RowIndex].Cells["nameKhoa"].Value.ToString();
                double DiemTB = Convert.ToDouble(dataGridView1.Rows[e.RowIndex].Cells["DiemTB"].Value);


            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                StudentContexDB context = new StudentContexDB();
                Student s = new Student()
                {
                    StudentID = textBox1.Text.Trim(),
                    FullName = textBox2.Text.Trim(),
                    AverageScore = double.Parse(textBox3.Text.Trim()),
                    FacultyID = int.Parse(comboBox1.SelectedValue.ToString())
                };
                context.Students.Add(s);
                context.SaveChanges();

                List<Student> listStudents = context.Students.Include("Faculty").ToList();
                BindGrid(listStudents);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                StudentContexDB context = new StudentContexDB();
                Student student = context.Students.FirstOrDefault(s => s.StudentID == textBox1.Text.Trim());
                if (student != null)
                {
                    student.FullName = textBox2.Text.Trim();
                    student.AverageScore = double.Parse(textBox3.Text.Trim());
                    student.FacultyID = int.Parse(comboBox1.SelectedValue.ToString());
                    context.SaveChanges();

                    List<Student> listStudents = context.Students.Include("Faculty").ToList();
                    BindGrid(listStudents);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                StudentContexDB context = new StudentContexDB();
                Student dbDelete = context.Students.FirstOrDefault(p => p.StudentID.ToString() == textBox1.Text.ToString());
                if (dbDelete != null)
                {
                    context.Students.Remove(dbDelete);
                    context.SaveChanges();
                    List<Student> listStudents = context.Students.ToList();
                    BindGrid(listStudents);
                }

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var ask = MessageBox.Show("Bạn có  muốn thoát ?", "Thoát", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ask == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void tìmKiếmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}

            
    

    

















