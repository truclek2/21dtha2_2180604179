﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.models;
using WindowsFormsApp1.report;

namespace WindowsFormsApp1
{
    public partial class reportv : Form
    {
        public reportv()
        {
            InitializeComponent();
        }

        private void reportv_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            {
                Model1 db = new Model1();
                var ListStudentReportdbo = db.Students.Select(c => new StudentReportDto
                {
                    StudentID = c.StudentID,
                    FullName = c.FullName,
                  FacultyName = c.Faculty.FacultyName,
                    AverageScore = c.AverageScore


                }).ToList();
                this.reportViewer1.LocalReport.ReportPath = "./report/Report1.rdlc" ;
                this.reportViewer1.RefreshReport();

                var reportDataSoure = new ReportDataSource("StudentDataSet", ListStudentReportdbo);
                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSoure);

                this.reportViewer1.RefreshReport();

            }
        }
    }
}
